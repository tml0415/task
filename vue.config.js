const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
    transpileDependencies: true,
    lintOnSave: false,//关闭语法检查
    publicPath: '/alatan/task/',
    outputDir: 'dist',
    assetsDir: 'assets',
    indexPath: 'index.html',
    filenameHashing: true,
    productionSourceMap: false,
    crossorigin: undefined, //
    integrity: false,
    parallel: require('os').cpus().length > 1,
    css: {
        // loaderOptions: {
        //     less: {
        //         lessOptions: {
        //         modifyVars: {
        //             'primary-color': '#1DA57A',
        //             'link-color': '#1DA57A',
        //             'border-radius-base': '2px',
        //         },
        //         javascriptEnabled: true,
        //         },
        //     },
        // },
    },
})
