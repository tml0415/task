import { createApp } from 'vue'
import App from './App.vue'
const app = createApp(App);
if (!global["vapp"]) { global["vapp"] = app }
import Antd from 'ant-design-vue'; 
app.use(Antd)
import mitt from 'mitt' //不同组件之间事件传递
app.config.globalProperties.$mitt = mitt()
import router from './js/router.js'
import store from './js/store.js'
import { axios_set } from './js/request.js'; axios_set()
import { apis_set } from './js/apis.js'; apis_set()
import { utils_set } from './js/utils.js'; utils_set()
import * as Icons from '@ant-design/icons-vue'
for (const i in Icons) {
    app.component(i, Icons[i])
}
import 'nprogress/nprogress.css';
import NProgress from 'nprogress';
NProgress.configure({
    showSpinner: false,
    parent: 'body',
    easing: "ease-in-out",
    speed: 300,
    trickleSpeed: 300,
});
app.config.globalProperties.$Loading = NProgress
app.use(router)
    .use(store)
// .use(createPinia())
.mount('#app');
window.onload = function () {
    console.log('window.onload');
    //VueApp.$mitt.emit('active_menu', VueApp.$route.name);
};