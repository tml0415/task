
import { createRouter, createWebHashHistory,createWebHistory, RouteRecordRaw } from 'vue-router';
const constantRoutes = [
  {
    path: '/',
    name: 'mytask',
    component: () => import('@/components/router/infos/mytask.vue'),
    meta: { requireAuth: true }
  },
  // {
  //   path: '/task',
  //   name: 'task',
  //   component: () => import('@/components/router/task.vue'),
  //   meta: { requireAuth: true }
  // },
  // {
  //   path: '/taskqc',
  //   name: 'taskqc',
  //   component: () => import('@/components/router/taskQualityCheck.vue'),
  //   meta: { requireAuth: true }
  // },
  // {
  //   path: '/taskfu',
  //   name: 'taskfu',
  //   component: () => import('@/components/router/task_fuzhi.vue'),
  //   meta: { requireAuth: true }

  // },
  // {
  //   path: '/task_fuzhi_quanzong',
  //   name: 'task_fuzhi_quanzong',
  //   component: () => import('@/components/router/task_fuzhi_quanzong.vue'),
  //   meta: { requireAuth: true }
  // },
  // {
  //   path: '/drawtask',
  //   name: 'drawtask',
  //   component: () => import('@/components/router/drawtask.vue'),
  //   meta: { requireAuth: true }
  // },
  // {
  //   path: '/manager',
  //   name: 'manager',
  //   component: () => import('@/components/router/manager/manager_main.vue'),
  //   meta: { requireAuth: true }
  // },
  
  // {
  //   path: '/checkai',
  //   name: 'checkai',
  //   component: () => import('@/components/router/check_ai/checkai.vue'),
  //   meta:{
  //     requireAuth:true
  //   }
  // },
  // {
  //   path: '/checkai_ys',
  //   name: 'checkai_ys',
  //   component: () => import('@/components/router/check_ai/checkai_ys.vue'),


  // },
  // {
  //   path: '/cog',
  //   name: 'cog',
  //   component: () => import('@/components/router/check_ai/checkai_cog.vue'),


  // },
  {
    path: '/aitask',
    name: 'aitask',
    component: () => import('@/components/router/aitask.vue'),
    meta: { requireAuth: true }
  },
  {
    path: '/mytask',
    name: 'mytask',
    component: () => import('@/components/router/infos/mytask.vue'),
    meta: { requireAuth: true }
  },
  {
    path: '/services',
    name: 'services',
    component: () => import('@/components/router/services/services.vue'),

  },
  {
    path: '/yx',
    name: 'yx',
    component: () => import('@/components/router/check_ai/yx.vue'),
  },
]
// 创建路由
const router = createRouter({
  history: createWebHistory("alatan/task"), //alatan/geopro
  // hash
  routes: constantRoutes ,
  // 刷新时，滚动条位置还原
  scrollBehavior: () => ({ left: 0, top: 0 })
});
// router.beforeEach(async (to, from, next) => {
//   if(to.path !='/'){
//     store.state.lastPath=to.path
//   }
//   let next_route=undefined
//   if(!store.state.token_object.token){
//     await app.config.globalProperties.$user.init();
//     app.config.globalProperties.$user_init=true
//   }
//   let _requireAuth=to?.meta?.requireAuth
//   if(_requireAuth===true){
//     let _true=await app.config.globalProperties.$user.read_user();
//     if(_true==false){
//       next_route={path:'/'}
//     }
//     else{
//       let get_user_detail=await app.config.globalProperties.$user.get_user_detail();
//       if(get_user_detail==false){ //token 过期
//         next_route={path:'/'}
//       }
//       else{
//         app.config.globalProperties.$user.getpic()
//       }
//     }
//   }
//   if(next_route){
//     next(next_route)
//   }
//   else{
//     next()
//   }
// })
export default router;
