class TurfClass{
    static CheckIntersect(polygon1 ,polygon2){
        return turf.booleanIntersects(polygon1, polygon2);
    }
    static CheckContains(polygon1 ,polygon2){
        return turf.booleanContains(polygon1, polygon2);
    }
    
    static ToUnion(polygon1 ,polygon2){
        return turf.union(polygon1, polygon2);
    }
    static ToPolygon(geometry,props){
        return turf.polygon(geometry, props);
    }
    static Check_Contain_Intersect(target_Coordinate,element_Coordinate){
        let target_polygon=TurfClass.ToPolygon(target_Coordinate)
        let element_polygon=TurfClass.ToPolygon(element_Coordinate)
        let is_contains_Crosses=false
        //判断是否包含
        is_contains_Crosses=TurfClass.CheckContains(target_polygon,element_polygon)//已经确定的多边形完全包含 新的多边形
        if(!is_contains_Crosses){
            //判断是否相交
            is_contains_Crosses=TurfClass.CheckIntersect(target_polygon,element_polygon)///已经确定的多边形完全与新的多边形相交
        }
        return is_contains_Crosses
    }
}
export default TurfClass;