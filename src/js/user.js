import store from './store.js';
class User{
    static set_token(token){
        store.state.user.token=token
    }
    static  init(){
        let UUMS_TOKEN=localStorage.getItem("UUMS_TOKEN");
        if(UUMS_TOKEN){
            let uums_json=JSON.parse(UUMS_TOKEN)
            let user=uums_json?.user
            let token=uums_json?.token

            console.log(uums_json)
        }
        console.log(UUMS_TOKEN)
        return false;
        store.state.user.token=token
    }
    static set_user(user){
        store.state.user.user=user


        //https://nmgwxyy.cn/loginByPassword?redirectURLhome=%2Falatan=%2Ftask=%2Fmytask
        //
    }
    static async check_token(){
        let _this=this;
        let refresh_token_Promise=new Promise((resolve) => {
            app.config.globalProperties.$axios.get("/refresh_token?",{ 
                params: {
                    username:_store.state.user.username,
                },
                auth:'true'
            })
            .then(res => {
                let BackData=res.data
                if (BackData.status == true) {
                    _store.state.user.token=BackData.result
                    resolve(true)
                }
                else{
                    resolve(false);
                }
            });
        })
        let result=await refresh_token_Promise
        if(result){
            result=await _this.write_user()
        }
        return result
    }
    // static init(_db='change',_version=1.0,_store='user',_key='name'){
    //     let _this=this;
    //     return new Promise((resolve) => {
    //         var request = window.indexedDB.open(_db, _version);
    //         request.onerror = (event)=> {
    //             resolve(false)
    //         };
    //         request.onsuccess =  (event)=> {
    //             _this.DB=request.result;
    //             resolve(true)
    //         };
    //         request.onupgradeneeded = (event)=> {
    //             _this.DB  = event.target.result;
    //             if (!_this.DB.objectStoreNames.contains(_store)) {
    //                 _this.DB.createObjectStore(_store, { keyPath: _key });
    //             }
    //             resolve(true)
    //         }
    //     });
    // }
    static read_user(_store='user',_key='name'){
        let _this=this;
        return new Promise((resolve ) => {
            let transaction = _this.DB.transaction([_store]);
            let objectStore = transaction.objectStore(_store);
            let request = objectStore.get(_key);
            request.onerror = (event)=>{
                resolve(false);
            };
            request.onsuccess = (event)=> {
                if (request.result) {
                    _store.state.user.token=request.result.value
                    resolve(true);
                } else {
                    resolve(false);
                }
            };
        });
    }
    static write_user(_store='user',_key='name'){
        let _this=this;
        return new Promise((resolve) => {
            let store=_this.DB.transaction([_store], 'readwrite').objectStore(_store);
            store.delete(_key)
            if(_store.state.user.token!=null){
                let request = store.add({ name: _key, value: _store.state.user.token });
                request.onsuccess = (event)=> {
                    resolve(true);
                };
                request.onerror = (event)=>{
                    resolve(false);
                }
            }
            else{
                _store.state.user.user=null
                _store.state.user.userpic=null
                resolve(true);
            }
            
        });
    }
    static async get_user_detail(){
        let _this=this;
        if(_store.state.user.user!=null){return true}
        let curuser_Promise=new Promise((resolve) => {
            app.config.globalProperties.$axios.get("/get_user_detail?",{ auth:'true'})
            .then(res => {
                let BackData=res.data
                if (BackData.status== true) {
                    _store.state.user.user=BackData.result
                    resolve(true)
                }
                else{
                    resolve(false);
                }
            });
        })
        let result=await curuser_Promise
        return result
    }
    static async get_user_by_role(roleid,org){
        let _this=this;
        if(_store.state.user.role_users!=null && _store.state.user.role_org==org){return true}
        let getIncludedUsers_Promise=new Promise((resolve) => {
            app.config.globalProperties.$axios.get("/get_user_by_role?",{ 
                params: {
                    roleid:roleid,
                    org:org
                },
                auth:'true'
            })
            .then(res => {
                let BackData=res.data
                if (BackData.status == true) {
                    _store.state.user.role_users=BackData.result
                    _store.state.user.role_org=org
                    resolve(true)
                }
                else{
                    resolve(false);
                }
            });

        });
        let result=await getIncludedUsers_Promise
        return result
    }
    static async get_user_by_org(org='2c9280857868c74b0178fdf3d6502225'){
        return new Promise((resolve) => {
            app.config.globalProperties.$axios.get("/get_user_by_org?",{ 
                params: {
                    org:org
                },
                auth:'true'
            })
            .then(res => {
                let BackData=res.data
                if (BackData.status == true) {
                    resolve(BackData.result)
                }
                else{
                    resolve(false);
                }
            });

        });
    }
    static async refresh_token(){
        let _this=this;
        let refresh_token_Promise=new Promise((resolve) => {
            app.config.globalProperties.$axios.get("/refresh_token?",{ 
                params: {
                    username:_store.state.user.username,
                },
                auth:'true'
            })
            .then(res => {
                let BackData=res.data
                if (BackData.status == true) {
                    _store.state.user.token=BackData.result
                    resolve(true)
                }
                else{
                    resolve(false);
                }
            });
        })
        let result=await refresh_token_Promise
        if(result){
            result=await _this.write_user()
        }
        return result
    }

    
    
    async getpic(){
        let _this=this;
        if(_store.state.user.token){
            let refresh_token_Promise=new Promise((resolve) => {
                app.config.globalProperties.$axios.get("/getpic?",{ 
                    auth:'true'
                })
                .then(res => {
                    let BackData=res.data
                    if (BackData.status == true) {
                        if(BackData.result){
                            _store.state.user.userpic=`data:image/png;base64,${BackData.result}`
                        }
                        resolve(true)
                    }
                    else{
                        resolve(false);
                    }
                });
            })
            let result=await refresh_token_Promise
            return result
        }
        else{
            return false
        }
    }
}
export function user_set(){
    global["vapp"].config.globalProperties.$user =User
  }