import { createStore } from 'vuex'
const store = createStore({
  state () {
    return {
      lastPath:'',
      token_object:{
        token:null,
        uums_token:"",
      },
      userpic:null,
      user:{
       
        // user:null,
        // username:null,
        // password:null,
        // 
      },
      user_roles:[],
      user_machine_room_levels:[],

      machine_room_xz:[
        {
          name:'互联网机房',
          val:'互联网机房',
        },
        {
          name:'局域网机房',
          val:'局域网机房',
        },
        {
          name:'保密机房',
          val:'保密机房',
        },
      ],
      machine_room_status:[
        {
          name:'运行中',
          val:'processing',
        },
        {
          name:'未启用',
          val:'default',
        }
      ],
      machine_type:[
        {
          name:'网络设备',
          val:'T01',
        },
        {
          name:'安全设备',
          val:'T02',
        },
        {
          name:'计算设备',
          val:'T03',
        },
        {
          name:'存储设备',
          val:'T04',
        },
        {
          name:'其他设备',
          val:'T05',
        },
      ],
      machine_status:[
        {
          name:'正常',
          val:'正常',
        },
        {
          name:'不正常',
          val:'不正常',
        },
        {
          name:'废弃',
          val:'废弃',
        },
      ],


      




      task_types:[
        {
          type:'bigdata',
          name:'执法服务',
        },
        {
          type:'sample',
          name:'AI样本',
        },
        {
          type:'jdcheck',
          name:'图斑检测',
        },
      ],
      bigdata_sample_works:[
        {
          type:'change',
          name:'变化判别',
        },
        {
          type:'changecheck',
          name:'变化判别检查',
        },
        {
          type:'edit',
          name:'图形编辑',
        },
        {
          type:'editcheck',
          name:'图形编辑检查',
        },
        // {
        //   type:'prop',
        //   name:'属性赋值',
        // },
        // {
        //   type:'propcheck',
        //   name:'属性赋值检查',
        // },
      ],

      role_users:null,
      role:'',



      orgs:[],
      roles:[],
      users:[],

      task_status:[],
      detail_status:[],
    }
  },
  mutations: {//同步
    
  },
  actions: { //异步

  }
})
export default store;