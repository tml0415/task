import store from './store';
// import dayjs from 'dayjs';
class common_apis{
  static read_xml_to_workbook(_file){
    return new Promise((resolve)=>{
      var fileReader = new FileReader();
      fileReader.onload = function(ev) {
        try {
          var data = ev.target.result
          var workbook = XLSX.read(data, {
            type: 'binary'
          }) // 以二进制流方式读取得到整份excel表格对象
          resolve(workbook)
        } catch (e) {
          resolve(false)
          console.log('文件类型不正确');
        }
      }
      fileReader.readAsBinaryString(_file);
    })
  }
  static read_workbook(workbook,sheetName)
  {
    // 表格的表格范围，可用于判断表头是否数量是否正确
    var fromTo = '';
    var rows = []; 
    if (workbook.Sheets.hasOwnProperty(sheetName)) {
      fromTo = workbook.Sheets[sheetName]['!ref'];
      rows = rows.concat(XLSX.utils.sheet_to_json(workbook.Sheets[sheetName]));
    }
    return rows
  }
  static excle_col_convert(rows)
  {
    let col_en={
      "序号":"key",
      "设备名称(固定资产名)":"name",
      "自编名":"aname",
      "机房编号":"jfbm",
      "固定资产号":"bh",
      "设备所在机柜存放位置":"wz",
      "管理者":"glz",
      "主要使用部门":"syz",
      "品牌":"pp",
      "型号":"xh",
      "序列号":"xlh",
      "入库时间":"sj",
      "设备类型":"type",
      "用途":"yt",
      "操作系统":"op",
      "设备状态":"status",
      "存储容量":"cc",
      "存储使用量":"ccsy",
      "其他说明":"sm",
    }
    let new_rows=[]
    for (let i = 0; i < rows.length; i++) {
      let element = rows[i];
      let new_element={}
      for (const [key, value] of Object.entries(element)) {
        let find_val=""
        for (const [key2, value2] of Object.entries(col_en)) {
          if(key2==key){
            find_val=value2
            break;
          }
        }
        if (find_val) {
          new_element[find_val]=value
        }
      }
      let net_basename="网络"
      let index=1
      let doit=true
      let nets=[]
      while(doit){
        let net_name=`${net_basename}${index}`
        if(element.hasOwnProperty(net_name)){
          let one_net={}
          one_net.net=net_name
          let ip_name=`${net_name}IP`
          if(element.hasOwnProperty(ip_name)){
            one_net.ip=element[ip_name]
          }
          let netdk_name=`${net_name}带宽`
          if(element.hasOwnProperty(netdk_name)){
            one_net.netdk=element[netdk_name]
          }
          let netlt_name=`${net_name}线缆类型`
          if(element.hasOwnProperty(netlt_name)){
            one_net.netlt=element[netlt_name]
          }
          let netpt_name=`${net_name}端口类型`
          if(element.hasOwnProperty(netpt_name)){
            one_net.netpt=element[netpt_name]
          }
          let ap_name=`${net_name}本端端口`
          if(element.hasOwnProperty(ap_name)){
            one_net.ap=element[ap_name]
          }
          let bname_name=`${net_name}对端设备`
          if(element.hasOwnProperty(bname_name)){
            one_net.bname=element[bname_name]
          }
          let bp_name=`${net_name}对端端口`
          if(element.hasOwnProperty(bp_name)){
            one_net.bp=element[bp_name]
          }
          nets.push(one_net)
          index=index+1
        }
        else{
          doit=false
        }
      }
      new_element.nets=nets
      new_rows.push(new_element)
    }
    return new_rows
  }
  static GetUserRole(_role)
    {
      let _roles=store.state?.user?.user?.roles
      if(_roles){
        let _role_index=_roles.findIndex((val)=>{
          return val==_role
        })
        if(_role_index>=0){
          return true
        }
      }
      return false
  }
  static Get_machine_room_level(machine_room_id){
    let _machine_room_index=store.state?.user_machine_room_levels?.findIndex((val)=>{
      return val?.id==machine_room_id
    })
    let level_bool=[false,false,false,false] 
    if(_machine_room_index>=0){
      let level=store.state?.user_machine_room_levels[_machine_room_index]?.level.split("")
      for (let j = 0; j < level.length; j++) {
        if(level[j]=="1"){
          level_bool[j]=true
        }
        else{
          level_bool[j]=false
        }
      }
    }
    return level_bool
  }
  static get_machine_type_by_Tnum(tnum){
    let _machine_room_index=store.state?.machine_type?.findIndex((val)=>{
      return val?.val==tnum
    })
    if(_machine_room_index>=0){
      return store.state?.machine_type[_machine_room_index]?.name
    }
    return ""
  }





  static GetTime(stime){
    let string_format="MM-dd hh:mm:ss"
    if(stime){
      return new Date(stime-0).format(string_format)
    }
    else{
      return "";
    }
  }
  static FormatFileSize(fileByte) {
    if(fileByte>0){
      var fileSizeByte = fileByte;
      var fileSizeMsg = "";
      if (fileSizeByte < 1048576) fileSizeMsg = (fileSizeByte / 1024).toFixed(2) + "KB";
      else if (fileSizeByte == 1048576) fileSizeMsg = "1MB";
      else if (fileSizeByte > 1048576 && fileSizeByte < 1073741824) fileSizeMsg = (fileSizeByte / (1024 * 1024)).toFixed(2) + "MB";
      else if (fileSizeByte > 1048576 && fileSizeByte == 1073741824) fileSizeMsg = "1GB";
      else if (fileSizeByte > 1073741824 && fileSizeByte < 1099511627776) fileSizeMsg = (fileSizeByte / (1024 * 1024 * 1024)).toFixed(2) + "GB";
      else fileSizeMsg = "文件超过1TB";
      return fileSizeMsg;
    }
    else{
      return ""
    }
    
  }
  static Get_img(preimage){
    if(preimage.indexOf(window.Glod.baseurl)>=0){
      return preimage
    }
    else{
      return `${window.Glod.baseurl}clip_merge/${preimage}`
    }
    
  }
  static Get_download_file(userid,pre_dir,name){
    return `${window.Glod.publicFileurl}${userid}${pre_dir=="/"?pre_dir:pre_dir+"/"}${name}` 
  }
  static Get_task_status(){
    if(store.state.task_status.length==0){
      this.axios_get("/get_task_status?").then(result => {
        store.state.task_status=result
      });
    }
  }
  static Get_detail_status(){
    if(store.state.detail_status.length==0){
      this.axios_get("/get_detail_status?").then(result => {
        store.state.detail_status=result
      });
    }
  }
  static Get_task_fuzhis(){
    if(store.state.task_status.length==0){
      this.axios_get("/get_task_fuzhis?").then(result => {
        store.state.task_fuzhis=result
      });
    }
  }
  static GetOrg(){
    this.axios_get("/get_org?",{},true).then(result => {
      if(result){
        store.state.orgs=result
      }
    });
  }
  static GetRole(){
    this.axios_get("/get_role?",{},true).then(result => {
      if(result){
        store.state.roles=result
      }
    });
  }
  static GetUser(org,role){
    if(!role){
      let _roles=[]
      for (let index = 0; index < store.state.roles.length; index++) {
        let element = store.state.roles[index];
        _roles.push(element.id)
      }
      role=_roles.join(",")
    }
    this.axios_get("/get_users?",{org,roles:role},true).then(result => {
      if(result){
        store.state.users=result
      }
    });
  }
  static Uuid(){
    return 'xxxxxxxx_xxxx_4xxx_yxxx_xxxxxxxx'.replace(/[xy]/g, function (c) {
        let r = Math.random() * 16 | 0,v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    })
  }
  static get_tif_time(_tifname){
    if(_tifname){
      let filename_splits=_tifname.split('_');
      for (let i = 0; i < filename_splits.length; i++)
      {
        let _time = filename_splits[i];
        try
        {
          let time_this= `${_time.substr(0, 4)}-${_time.substr(4, 2)}-${_time.substr(6, 2)}`
          let date=new Date(time_this);
          if(date instanceof Date){
            let timeticks=date.getTime()
            if(!isNaN(timeticks)){
              if(timeticks>1262275200000){
                return time_this;
              }
            }
          }
        }
        catch
        {
            continue;
        }
      }
    }
    
    return "0000-00-00";
  }
  static Get_task_type_name(task_type){
    let task_type_index=store.state.task_types.findIndex((val)=>{
      return val.type==task_type
    })
    return task_type_index>=0?store.state.task_types[task_type_index].name:""
  }
  static Get_work_type_name(work_type){
    let task_type_index=store.state.bigdata_sample_works.findIndex((val)=>{
      return val.type==work_type
    })
    return task_type_index>=0?store.state.bigdata_sample_works[task_type_index].name:""
  }
}
export function utils_set(){
  global["vapp"].config.globalProperties.$common_apis =common_apis
}
