class APIS{
  static async check_token(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_post("/change/check_token?", qs, false, false)
  }
  static async get_user_pic(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_get("/change/getpic?", qs, true, true)
  }
  static async get_user_role(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_post("/change/get_user_role?", qs, true, true)
  }
  static async get_role_users(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_post("/change/get_role_users?", qs, true, true)
  }
  static async get_room_level(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_get("/change/get_level?", qs, true, true)
  }
  static async update_room_level(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_post("/change/update_level?", qs, true, true)
  }
  static async delete_room_level(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_post("/change/delete_level?", qs, true, true)
  }

  
  
  static async get_year(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_get("/change/get_year?", qs, true, true)
  }
  static async add_year(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_get("/change/add_year?", qs, true, true)
  }
  static async get_pro(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_get("/change/get_pro?", qs, true, true)
  }
  static async add_pro(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_post("/change/add_pro?", qs, true, true)
  }
  static async delete_pro(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_post("/change/delete_pro?", qs, true, true)
  }
  static async add_group(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_post("/change/add_group?", qs, true, true)
  }
  static async edit_group(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_post("/change/edit_group?", qs, true, true)
  }
  static async get_group(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_get("/change/get_group?", qs, true, true)
  }
  static async delete_group(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_post("/change/delete_group?", qs, true, true)
  }
  static async add_file(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_post("/change/add_file?", qs, true, true)
  }
  static async get_file(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_get("/change/get_file?", qs, true, true)
  }
  static async delete_file(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_post("/change/delete_file?", qs, true, true)
  }

  static async get_machine_room(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_get("/change/get_machine_room?", qs, true, true)
  }
  static async add_machine_room(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_post("/change/add_machine_room?", qs, true, true)
  }
  static async get_machine(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_get("/change/get_machine?", qs, true, true)
  }
  static async add_machine(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_post("/change/add_machine?", qs, true, true)
  }
  static async delete_machine(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_post("/change/delete_machine?", qs, true, true)
  }
  static async delete_machines(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_post("/change/delete_machines?", qs, true, true)
  }

  









  
  static async get_ping(url,qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_get(url ? url + "/ping?" :"/ping?", qs, false, true,100)
  }
  static async get_config_resolution(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_get("/task/config/get_config_resolution?", qs, false, true)
  }
  static async get_config_domdem(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_get("/task/config/get_config_domdem?", qs, false, true)
  }
  static async get_config_satellite(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_get("/task/config/get_config_satellite?", qs, false, true)
  }
  static async get_config_parmars(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_get("/task/config/get_config_parmars?", qs, false, true)
  }
  static async get_dsm_parmars(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_get("/task/config/get_dsm_parmars?", qs, false, true)
  }
  static async set_config_resolution(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_post("/task/config/set_config_resolution?", qs, false, false)
  }
  static async set_config_domdem(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_post("/task/config/set_config_domdem?", qs, false, false)
  }
  static async set_config_parmars(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_post("/task/config/set_config_parmars?", qs, false, false)
  }
  static async set_dsm_parmars(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_post("/task/config/set_dsm_parmars?", qs, false, false)
  }
  static async get_exe_progress(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_get("/task/get_exe_progress?", qs, false, true)
  }
  static async get_exetask(qs={}){
    return await global["vapp"].config.globalProperties.$axios_get("/task/get_exetask?",qs,false,true)
  }
  static async get_exetask_log(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_get("/task/get_exetask_log?", qs, false, false)
  }
  static async retry_exetask(qs={}){
    return await global["vapp"].config.globalProperties.$axios_get("/task/retry_exetask?", qs, false, false)
  }
  static async delete_exetask(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_get("/task/delete_exetask?", qs, false, false)
  }
  static async cancle_exetask(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_get("/task/cancle_exetask?", qs, false, false)
  }
  static async delete_task(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_post("/task/delete_task?", qs, false, false)
  }
  static async clear_task(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_post("/task/clear_task?", qs, false, false)
  }


  
  static async get_flowtask(qs={}){
    return await global["vapp"].config.globalProperties.$axios_get("/task/get_flow?",qs,false,true)
  }
  static async delete_flowtask(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_post("/task/delete_flow?", qs, false, false)
  }
  static async retry_flowtask(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_post("/task/retry_flow?", qs, false, false)
  }
  static async get_ctask(qs={}){
    return await global["vapp"].config.globalProperties.$axios_get("/task/get_ctask?",qs,false,true)
  }


  
  static async add_task_bcmatch(qs={}){
    return await global["vapp"].config.globalProperties.$axios_post("/task/add_task_bcmatch?",qs,false,true)
  }

  
  static async get_task(qs={}){
    return await global["vapp"].config.globalProperties.$axios_get("/task/get_task?",qs,false,true)
  }

  static async get_nodes(qs={}){
    return await global["vapp"].config.globalProperties.$axios_get("/task/get_nodes?",qs,false,true)
  }

  static async check_tifs(qs={}){
    return await global["vapp"].config.globalProperties.$axios_get("/task/check_tifs?", qs, false, false)
  }
  static async check_tars(qs={}){
    return await global["vapp"].config.globalProperties.$axios_get("/task/check_tars?", qs, false, false)
  }

  
  static async add_task(qs={}){
    return await global["vapp"].config.globalProperties.$axios_post("/task/add_task?", qs, false, false)
  }

  static async get_files(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_post("/task/config/get_files?", qs, false, true)
  }
  static async get_disks(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_get("/task/config/get_disks?", qs, false, true)
  }
  static async mk_dir(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_post("/task/config/mk_dir?", qs, false, false)
  }
  static async get_ctask_coords(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_get("/task/get_ctask_coords?", qs, false, true)
  }
  static async get_ctask_coords_by_index(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_get("/task/get_ctask_coords_by_index?", qs, false, false)
  }
  static async get_ctask_charts(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_get("/task/get_ctask_charts?", qs, false, true)
  }
  static async get_static_img(url) {
    return await global["vapp"].config.globalProperties.$axios_get(url,{}, false, false)
  }

  //刺点
  static async point_add_task(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_post("/task/point/add_task?", qs, false, true)
  }
  static async point_get_details_by_lonlat(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_get("/task/point/get_details_by_lonlat?", qs, false, true)
  }
  static async point_get_z_by_lonlat(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_get("/task/point/get_z?", qs, false, true)
  }
  static async point_add_points(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_post("/task/point/add_points?", qs, false, true)
  }
  static async point_get_points(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_get("/task/point/get_points?", qs, false, true)
  }

  
  static async point_update_point(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_post("/task/point/update_point?", qs, false, true)
  }
  static async point_delete_point(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_post("/task/point/delete_point?", qs, false, true)
  }
  static async point_get_point(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_get("/task/point/get_point?", qs, false, true)
  }
  static async point_reset_point(qs = {}) {
    return await global["vapp"].config.globalProperties.$axios_get("/task/point/reset_point?", qs, false, true)
  }
}
export function apis_set(){
  global["vapp"].config.globalProperties.$apis =APIS
}